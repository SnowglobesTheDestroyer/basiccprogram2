#include "myBank.h"
#include <stdio.h>

#define BANK_ROWS 50  //bank array rows
#define BANK_COLS 2   //bank array columns
#define ACC_NUM_MAX 950   //max account number
#define ACC_NUM_MIN 901   //min account number

double arr_bank_acc[BANK_ROWS][BANK_COLS];

void open_account(double amount)
{
  int c = 0;
  for (int i = 0;i < BANK_ROWS; i++)
  {
    if (arr_bank_acc[i][0] == 0)
    {
      arr_bank_acc[i][1] = amount;
      break;
    }
    c = i;
  }
  if (c >= 50)
  {
    printf("We are very sorry, the bank cannot create more accounts.\n");
    printf("Please go to our offices and request for help.\n");
  }
  else
  {
    printf("Account created successfully.\nYour account number is %d \n", ACC_NUM_MIN + c);
  }
}

void close_account(int accNum)
{
  if (arr_bank_acc[accNum][0] == 1) {
    arr_bank_acc[accNum][0] = 0;
    arr_bank_acc[accNum][1] = 0;
    printf("Account %d successfully closed.\n", accNum + ACC_NUM_MIN);
  }
  else{
    printf("ERROR: Account %d cannot be closed.\nThis account number does not corresspond to any existing account\n", accNum + ACC_NUM_MIN);
  }
}

void print_balance(int accNum)
{
  printf("The balance in your account is %.2f\n", arr_bank_acc[accNum][1]);
}

void add_deposit(int accNum, double amount)
{
  arr_bank_acc[accNum][1] += amount;
  print_balance(accNum);
}

void sub_withdrawal(int accNum, double amount)
{
  if (amount < arr_bank_acc[accNum][1])
  {
    printf("Insufficient funds\n");
  }
  else
  {
    arr_bank_acc[accNum][1] -= amount;
  }
  print_balance(accNum);
}

void add_interest(double rate)
{
  rate = (rate/100.0) + 1.0;
  for (int i = 0; i < BANK_ROWS; i++) {
    if (arr_bank_acc[i][0] == 1)
    {
      arr_bank_acc[i][1] *= rate;
    }
  }
  printf("Successfully applied interest of %.2lf percent to all accounts.\n", rate);
}

void print_allOpenAccounts()
{
  printf("Printing details of all open accounts:\n");
  for (int i = 0; i < BANK_ROWS; i++) {
    if (arr_bank_acc[i][0] == 1)
    {
      printf("The balance in account %d is %.2f\n", i+ACC_NUM_MIN, arr_bank_acc[i][1]);
    }
  }
}

void close_allAccounts()
{
    for (int i=0; i<BANK_ROWS;i++) {
        arr_bank_acc[i][0] = 0;
        arr_bank_acc[i][1] = 0;
    }
}
