#include <stdio.h>
#include "myBank.h"
#define ACC_NUM_MAX 950   //max account number
#define ACC_NUM_MIN 901   //min account number
int get_accountNumber()
{
  int accNum;
  int i = 1;
  while(i){
    scanf("Please write your account number: %d\n", &accNum);
    if (accNum < ACC_NUM_MIN || ACC_NUM_MAX < accNum) {
      printf("%d is an invalid account number.\n", accNum);
    }
    else{
      i = 0;
    }
  }
  accNum -= ACC_NUM_MIN;
  return accNum;
}

double get_amount()
{
  double amount = 0;
  int k = 1;
  while(k == 1)
  {
    scanf("How much? %lf\n", &amount);
    if (amount < 0)
    {
      printf("Invalid amount.\n");
    }
    else
    {
      k = 0;
    }
  }
  return amount;
}


int main(){
  char ch;
  int accNum = 0;
  double amount = 0;
  printf("Hello,\n welcome to the international Service for Online Banking\nAlso known as the iSOB.\n");
  while (ch != 'E') {
      printf("Here are the following commands this service provides:\n");
      printf(" O - [O]pens a new account\n B - print your account [B]alance\n D - [D]eposit funds to your account\n W - [W]ithdraw funds from your account\n C - [C]lose your account\n I - add an [I]nterest to all accounts\n P - [P]rint details of all open accounts\n E - [E]xit iSOB\n");


      if(scanf("%c", &ch) != 1) {
          printf("Error has been made, please try again\n");
      }

      else {
          switch (ch) {
              case 'O':
                  printf("\nYou have selected Open account:\n");
                  amount = get_amount();
                  open_account(amount);
                  break;
              case 'B' :
                  printf("\nYou have selected print account Balance:\n");
                  accNum = get_accountNumber();
                  print_balance(accNum);
                  break;

              case 'D' :
                  printf("\nYou have selected Deposit funds\n");
                  accNum = get_accountNumber();
                  amount = get_amount();
                  add_deposit(accNum, amount);
                  break;

              case 'W' :
                  printf("\nYou have selected Withdraw funds\n");
                  accNum = get_accountNumber();
                  amount = get_amount();
                  sub_withdrawal(accNum, amount);
                  break;

              case 'C' :
                  printf("\nYou have selected Close account:\n");
                  accNum = get_accountNumber();
                  close_account(accNum);
                  break;

              case 'I' :
                  printf("\nYou have selected Interest\n");
                  amount = 0;
                  scanf("What is the interest rate (in percentage)? %lf\n", &amount);
                  add_interest(amount);
                  break;

              case 'P':
                  print_allOpenAccounts();
                  break;

              case 'E' :
                  close_allAccounts();
                  printf("\nSad to see you go.\nWill you come back to play again?\nokay then....bye... (T.T)\n");
                  break;

              default:
                  printf("Error: Invalid input.\nPlease type a valid input from the list below.\n");
                  break;

          }
      }
  }
}
