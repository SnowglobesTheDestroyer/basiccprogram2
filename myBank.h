#ifndef MYBANK_H
#define MYBANK_H


void open_account();

void close_account();

void print_balance();

void add_deposit();

void sub_withdrawal();

void add_interest();

void print_allOpenAccounts();

void close_allAccounts();

#endif
